package mostrandodatos;

import java.util.Scanner;

public class MostrandoDatos {

	public static void main(String[] args) {
	
	Scanner in = new Scanner (System.in);
		
	String name;
	String surname;
	
	System.out.println("Name:");
	name = in.nextLine();
	System.out.println("Surname:");
	surname = in.nextLine();
	
	System.out.println(name + " " + surname);

	in.close();
	
	}
}
